package org.vivoweb.webapp.controller.freemarker;

import edu.cornell.mannlib.vitro.webapp.controller.VitroRequest;
import edu.cornell.mannlib.vitro.webapp.controller.freemarker.FreemarkerHttpServlet;
import edu.cornell.mannlib.vitro.webapp.controller.freemarker.responsevalues.ResponseValues;
import edu.cornell.mannlib.vitro.webapp.controller.freemarker.responsevalues.TemplateResponseValues;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

@WebServlet("/dataprotection/*")
public class DataProtectionController extends FreemarkerHttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    protected ResponseValues processRequest(VitroRequest vreq) {

        return new TemplateResponseValues("dataprotection.ftl");

    }
}