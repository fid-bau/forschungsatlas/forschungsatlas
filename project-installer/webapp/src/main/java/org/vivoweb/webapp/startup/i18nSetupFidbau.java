package org.vivoweb.webapp.startup;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import edu.cornell.mannlib.vitro.webapp.i18n.VitroResourceBundle;

public class i18nSetupFidbau implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        VitroResourceBundle.addAppPrefix("fidbau");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}
