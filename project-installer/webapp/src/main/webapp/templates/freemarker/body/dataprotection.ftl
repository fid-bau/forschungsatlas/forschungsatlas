
<h2>${i18n().dataprotection_headline}</h2>
<p style="margin-bottom:1em;">${i18n().text_dataprotection_1st}</p>
<p style="margin-bottom:1em;">${i18n().text_dataprotection_2nd}</p>
<p style="margin-bottom:2em;">${i18n().text_dataprotection_3rd}</p>

<h3>${i18n().responsible_dataprotection_headline}</h3>
<p style="margin-bottom:1em;">${i18n().responsible_dataprotection_subtitle}</p>
<p style="margin-bottom:1em;">${i18n().responsible_dataprotection_address}</p>
<p style="margin-bottom:2em;">${i18n().responsible_dataprotection_mailphone}</p>

<h3>${i18n().dataprotectionofficer_headline}</h3>
<p style="margin-bottom:1em;">${i18n().dataprotectionofficer_subtitle}</p>
<p style="margin-bottom:1em;">
    ${i18n().dataprotectionofficer_name}<br>
    ${i18n().dataprotectionofficer_phone}<br>
    ${i18n().dataprotectionofficer_mail}
</p>

<h4>${i18n().address_headline}</h4>
<p style="margin-bottom:1em;">${i18n().dataprotectionofficer_address}</p>

<h4>${i18n().visitors_address_headline}</h4>
<p style="margin-bottom:1em;">${i18n().visitors_address}</p>
<p style="margin-bottom:2em;">${i18n().dataprotection_questions}</p>

<h3>${i18n().registration_for_Services_headline}</h3>
<p style="margin-bottom:1em;">${i18n().registration_for_Services_para1}</p>
<p style="margin-bottom:2em;">${i18n().registration_for_Services_para2}</p>

<h3>${i18n().contact_option_headline}</h3>
<p style="margin-bottom:2em;">${i18n().contact_option_para1}</p>

<h3>${i18n().cookies_headline}</h3>
<p style="margin-bottom:1em;">${i18n().cookies_para1}</p>
<p style="margin-bottom:1em;">${i18n().cookies_para2}</p>
<p style="margin-bottom:2em;">${i18n().cookies_para3}</p>

<h3>${i18n().web_analysis_tools_headline}</h3>
<p style="margin-bottom:1em;">${i18n().web_analysis_tools_para1}</p>
<p style="margin-bottom:1em;">${i18n().web_analysis_tools_para2}</p>
<p style="margin-bottom:2em;">${i18n().web_analysis_tools_para3}</p>

<h3>${i18n().matomo_opt_out_headline}</h3>
<p style="margin-bottom:1em;">${i18n().matomo_opt_out_para1}</p>
<p style="margin-bottom:2em;">${i18n().matomo_opt_out_para2}</p>

<h3>${i18n().collection_of_data_headline}</h3>
<p style="margin-bottom:1em;">${i18n().collection_of_data_para1}</p>
<ul style="margin-left: 40px; margin-bottom:1em; list-style-type:circle;" >
    <li>${i18n().collection_of_data_bulletpoint1}</li>
    <li>${i18n().collection_of_data_bulletpoint2}</li>
    <li>${i18n().collection_of_data_bulletpoint3}</li>
    <li>${i18n().collection_of_data_bulletpoint4}</li>
    <li>${i18n().collection_of_data_bulletpoint5}</li>
    <li>${i18n().collection_of_data_bulletpoint6}</li>
    <li>${i18n().collection_of_data_bulletpoint7}</li>
</ul>
<p style="margin-bottom:2em;">${i18n().collection_of_data_para2}</p>

<h3>${i18n().routine_deletion_headline}</h3>
<p style="margin-bottom:2em;">${i18n().routine_deletion_para1}</p>

<h3>${i18n().rights_of_the_data_subject_headline}</h3>
<p style="margin-bottom:2em;">${i18n().rights_of_the_data_subject_para1}</p>