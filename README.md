# Forschungsatlas
This repository contains the source of the [Forschungsatlas](https://forschungsatlas.fid-bau.de/), one output of the [FID BAUdigital](https://www.fid-bau.de/).  

It's using  a git repository template for working with and customizing [VIVO](http://vivoweb.org/).  It uses the [three tiered build approach](https://wiki.duraspace.org/display/VIVO/Building+VIVO+in+3+tiers) documented by the VIVO project.  The project source files (VIVO and Vitro) are tracked using [Git Submodules](http://git-scm.com/book/en/Git-Tools-Submodules).

For a more detailed explanation of setting up the VIVO environment, consult the
[VIVO version 1.12 installation instructions](https://wiki.lyrasis.org/display/VIVODOC112x/Installing+VIVO).

# FID BAUdigital
The Specialised Information Service BAUdigital (FID BAUdigital) – funded by the German Research Foundation (DFG) – is being established as a sustainable digital platform to support research in the field of built environment and building technology. BAUdigital will be developed in direct dialogue with the specialist community and a scientific advisory board, through various workshops and an international conference. The aim is to provide jointly designed future-oriented services.

# How to check out the project and build VIVO in three tiers

### Develop branch
~~~

    git clone https://git.tib.eu/OSL/VIVO/VIVO_project_template.git vivo
    cd vivo
    git submodule init

#Pull in VIVO, VIVO-languages, Vitro and Vitro-languages.  This will take a few minutes.

    git submodule update

#Check out specific versions of VIVO and Vitro

    cd VIVO
    git checkout develop-arm
    cd ../Vitro
    git checkout develop-arm-vqt

#Change back to vivo main directory

    cd ../VIVO


#Build and deploy

mvn install -s ../project-settings.xml
